import React from "react";
import "../Home-Page/Home.css";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import ReactPlayer from "react-player";
import { Chart } from "react-charts";
import Header from "../Components/Header";
import CheckMarketPrices from "../Components/CheckMarketPrices";
import MandiRates from "../Components/MandiRates";
import VideoContainer from "../Components/VideoContainer";
import VideoSection from "../Components/VideoSection";
import Headlines from "../Components/Headlines";
import Form from "../Components/Form";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import img1 from "../images/1.png";
import img2 from "../images/2.jpg";
import img3 from "../images/3.png";

class App extends React.Component {
  render() {
    // static chart

    const data = [
      {
        data: [{ x: 20, y: 100 }],
      },
      {
        data: [{ x: 40, y: 85 }],
      },
      {
        data: [{ x: 60, y: 95 }],
      },
    ];
    const axes = [
      { primary: true, type: "linear", position: "bottom" },
      { type: "linear", position: "left" },
    ];

    return (
      <div>
        {/* top header */}
        <div style={{ backgroundColor: "#390400" }}>
          <Header />
        </div>

        {/* carousel */}
        <div>
          <Carousel showThumbs={false} infiniteLoop={true}>
            <div
              style={{ height: "450px", backgroundColor: "rgb(255, 155, 41)" }}
            >
              <img style={{ height: "450px", width: "450px" }} src={img1} />
            </div>
            <div
              style={{ height: "450px", backgroundColor: "rgb(255, 155, 41)" }}
            >
              <img style={{ height: "450px", width: "450px" }} src={img2} />
            </div>
            <div
              style={{ height: "450px", backgroundColor: "rgb(255, 155, 41)" }}
            >
              <img style={{ height: "450px", width: "450px" }} src={img3} />
            </div>
          </Carousel>
        </div>

        {/* menu */}
        <div className="menu">
          <div className="menuitem">
            <a href="#" style={{ color: "white" }}>
              Home
            </a>
            <span className="pipe">|</span>
            <a href="#" style={{ color: "white" }}>
              Poultry Markets
            </a>
            <span className="pipe">|</span>
            <a href="#" style={{ color: "white" }}>
              Videos
            </a>
            <span className="pipe">|</span>
            <a href="#" style={{ color: "white" }}>
              News
            </a>
            <span className="pipe">|</span>
            <a href="#" style={{ color: "white" }}>
              Articles
            </a>
            <span className="pipe">|</span>
            <a href="#" style={{ color: "white" }}>
              Doctor's Concern
            </a>
            <span className="pipe">|</span>
            <a href="#" style={{ color: "white" }}>
              Tip of the Day
            </a>
            <span className="pipe">|</span>
            <a href="#" style={{ color: "white" }}>
              FAQ's
            </a>
          </div>
        </div>

        {/* adds */}
        <div className="adds">
          <marquee direction="left">Poultry Baba App Coming Soon</marquee>
        </div>

        {/* poultry videos images and one article */}
        <div style={{ backgroundColor: "rgb(255,155,41)" }}>
          <CheckMarketPrices />
        </div>

        {/* Live egg rates with slider */}
        <div className="live-egg-rates">
          <div>
            <div className="text-div">
              <p className="text-inside">Live Egg Rate & Mandi on 08-08-2020</p>
            </div>

            {/* horizontal line */}
            <hr className="empty-div"></hr>
            <Carousel showStatus={false} infiniteLoop={true}>
              <div style={{ backgroundColor: "white" }}>
                <MandiRates />
              </div>
              <div style={{ backgroundColor: "white" }}>
                <MandiRates />
              </div>
            </Carousel>
          </div>
        </div>

        {/* static charts */}

        <div className="static-chart">
          <div
            style={{
              marginLeft: "15%",
              paddingTop: "50px",
              width: "70%",
              height: "500px",
            }}
          >
            <Chart data={data} axes={axes} />
          </div>
        </div>

        {/* adds */}

        <div className="adds">
          <marquee direction="left">Poultry Baba App Coming Soon</marquee>
        </div>

        {/* video cards in carousel */}
        <div style={{ backgroundColor: "rgb(255, 155, 41)" }}>
          <Carousel showStatus={false} showIndicators={false}>
            <div style={{ backgroundColor: "rgb(255,155,41)" }}>
              <VideoContainer />
            </div>
            <div style={{ backgroundColor: "rgb(255,155,41)" }}>
              <VideoContainer />
            </div>
          </Carousel>
        </div>

        {/* videos adds with description div */}
        <div style={{ backgroundColor: "#d3d3d3", marginTop: "-20px" }}>
          <VideoSection />
        </div>

        {/* Headlines  */}
        <div style={{ backgroundColor: "orange" }}>
          <Headlines />
        </div>

        {/* adds */}
        <div className="adds">
          <marquee direction="left">Poultry Baba App Coming Soon</marquee>
        </div>

        {/* Tip of the Day & Doctor's Concern */}

        <div style={{ backgroundColor: "rgb(255,155,41)" }}>
          <Container>
            <Row>
              <Col style={{ backgroundColor: "rgb(255,155,41)" }}>
                <h1 className="tipofTheDayHeading">Tip of The Day</h1>
                <h3 className="headline">Headline</h3>
                <ReactPlayer
                  controls
                  width="auto"
                  height="250px"
                  url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
                ></ReactPlayer>
                <p style={{ paddingTop: "20px" }}>
                  The Federation of Pakistan Chambers of Commerce and Industry
                  (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                  Khan to help the poor combat poverty through poultry business
                  The Federation of Pakistan Chambers of Commerce and Industry
                  (FPCCI) on Wednesday lauded the move of Prime Minister{" "}
                </p>
              </Col>
              <Col style={{ backgroundColor: "lightblue" }}>
                <h1 className="doctorsConcernHeading">Doctor's Concern</h1>
                <h3 className="headline">Headline</h3>
                <ReactPlayer
                  controls
                  width="auto"
                  height="250px"
                  url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
                ></ReactPlayer>
                <p style={{ paddingTop: "20px" }}>
                  The Federation of Pakistan Chambers of Commerce and Industry
                  (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                  Khan to help the poor combat poverty through poultry business
                  The Federation of Pakistan Chambers of Commerce and Industry
                  (FPCCI) on Wednesday lauded the move of Prime Minister{" "}
                </p>
              </Col>
            </Row>
          </Container>
        </div>

        {/* simple text div */}

        <div className="simpleTextDiv">
          <p
            style={{
              textAlign: "center",
              fontFamily: "times-new-roman",
              fontSize: "large",
            }}
          >
            Share news videos with us and have them featured at our website!
          </p>
        </div>

        {/* subscribe button  */}
        <div className="subscribeButtonDiv">
          <div style={{ paddingTop: "25px" }} class="col">
            <p className="textStyleInBecomeAMember">
              Become a Member of Poultry Baba
            </p>
            <button type="submit" className="subscribeButton">
              Subscribe Now
            </button>
          </div>
        </div>



        {/* Validation form div */}
        <div>
          <Form />
        </div>



        {/* footer Menu */}
        <div>
          <div className="footerMenu">
            <div className="footerMenuitem">
              <a style={{ fontSize: "large", color: "white" }} href="#">
                Home
              </a>
              <span className="pipe">|</span>
              <a style={{ fontSize: "large", color: "white" }} href="#">
                Poultry Markets
              </a>
              <span className="pipe">|</span>
              <a style={{ fontSize: "large", color: "white" }} href="#">
                News
              </a>
              <span className="pipe">|</span>
              <a style={{ fontSize: "large", color: "white" }} href="#">
                Articles
              </a>
              <span className="pipe">|</span>
              <a style={{ fontSize: "large", color: "white" }} href="#">
                Doctor's Concern
              </a>
              <span className="pipe">|</span>
              <a style={{ fontSize: "large", color: "white" }} href="#">
                FAQ's
              </a>
            </div>
            <div className="copyrightDiv">
              <p className="CopyrightStyle">
                {" "}
                Copyright © 2018 PoultryBaba. All Rights Reserved
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default App;
