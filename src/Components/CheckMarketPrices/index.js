import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ReactPlayer from "react-player";



import img4 from "../CheckMarketPrices/egg1.png";
import img5 from "../CheckMarketPrices/baby chicken.jpg";
import img6 from "../CheckMarketPrices/broiler chicken.png";

class CheckMarketPrices extends React.Component {
  render() {
    return (
      <div>
        <Row>
          <Col sm={8}>
            <div className="market-price-text">
              <p className="market-price-heading">
                {" "}
                Check Market Prices - 2020{" "}
              </p>
            </div>

            <div className="clipart-div">
              <div>
                <img style={{ width: "80%", height: "auto" }} src={img4} />
                <button type="button" className="clipartDivButton1">
                  Egg Rate
                </button>
              </div>
              <div>
                <img style={{ width: "60%", height: "auto" }} src={img5} />
                <button type="button" className="clipartDivButton2">
                  Chicken Rate
                </button>
              </div>
              <div>
                <img style={{ width: "57%", height: "auto" }} src={img6} />
                <button type="button" className="clipartDivButton3">
                  Layer | Broiler
                </button>
              </div>
            </div>

            <div>
              <div className="text-in-article-div">
                <h1 className="heading-style">Broiler Market Prices - 2020</h1>
                <div className="empty-div-style"></div>
                <p className="paragraph-style">
                  The market rates are disseminated for information only and
                  shall not be considered as guidance, invitation or persuasion.
                  Users/Visitors have to make their own decisions based on their
                  own independent enquiries, appraisals, judgement, wisdom and
                  risks. AGBRO and its afﬁliates, or their employees, directors
                  or agents shall not be liable or responsible for any loss or
                  costs or any action whatsoever arising out of use or relying
                  on the prices disseminated.
                </p>
                <h1 className="another-heading-style">
                  All prices quoted in Pakistani Rupees
                </h1>
                <p className="another-paragraph-style">
                  * DOC – Day-Old Chick <br />
                  Farm Rates and Mandi open and closing rates are per
                  <br /> kilogram live weight
                </p>
              </div>
            </div>
          </Col>


          <Col sm={4} style={{paddingRight:"20px",paddingTop:"20px",marginLeft:"-30px"}}>
            <div className="live-video-div">
              <ReactPlayer
                controls
                width="80%"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                  paddingTop: "50px",
                }}
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              />
              <h6 style={{ marginLeft: "10%", marginRight: "10%" }}>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....(ReadULTIMATE){" "}
              </h6>

              <ReactPlayer
                controls
                width="80%"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                  paddingTop: "50px",
                }}
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              />
              <h6 style={{ marginLeft: "10%", marginRight: "10%" }}>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....(ReadULTIMATE){" "}
              </h6>

              <ReactPlayer
                controls
                width="80%"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                  paddingTop: "50px",
                }}
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              />
              <h6 style={{ marginLeft: "10%", marginRight: "10%" }}>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....(ReadULTIMATE){" "}
              </h6>

              <ReactPlayer
                controls
                width="80%"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                  paddingTop: "50px",
                }}
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              />
              <h6 style={{ marginLeft: "10%", marginRight: "10%" }}>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....(ReadULTIMATE){" "}
              </h6>

              <ReactPlayer
                controls
                width="80%"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                  paddingTop: "50px",
                }}
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              />
              <h6 style={{ marginLeft: "10%", marginRight: "10%" }}>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....The Federation of Pakistan Chambers of Commerce and
                Industry (FPCCI) on Wednesday lauded the move of Prime Minister
                Imran Khan to help the poor combat poverty through poultry
                business....(ReadULTIMATE){" "}
              </h6>
            </div>{" "}

          </Col>
        </Row>
      </div>
    );
  }
}

export default CheckMarketPrices;
