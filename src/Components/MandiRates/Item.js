import React from "react";

const RateItem = (props) => {
  const { city } = props;
  const { formGateRate } = props;
  const { rate } = props;
  const { mandiGurus } = props;
  const { rate1 } = props;
  const { marketNow } = props;
  const { rate2 } = props;
  const { foreCast } = props;
  const { rate3 } = props;
  return (
    <div>
      <div
        style={{
          padding: "5px",
          backgroundColor: "crimson",
          color: "white",
          borderRadius: "30px",
        }}
      >
        <h1
          style={{
            backgroundColor: "lightsalmon",
            borderRadius: "10px",
            textAlign: "center",
            marginTop: "5px",
          }}
        >
          {city}
        </h1>
        <h4 style={{ textAlign: "center",fontFamily:"times-new-roman",fontWeight:"bolder" }}>{formGateRate}</h4>
        <h1 style={{ textAlign: "center",fontFamily:"times-new-roman",fontSize:"50px",fontWeight:"bolder" }}>{rate}</h1>
        <h5 style={{ textAlign: "center", color: "yellow",fontFamily:"times-new-roman",fontWeight:"bolder" }}>{mandiGurus}</h5>
        <h4 style={{ textAlign: "center" ,fontFamily:"times-new-roman",fontWeight:"bolder"}}>{rate1}</h4>
        <h5 style={{ textAlign: "center", color: "yellow",fontFamily:"times-new-roman",fontWeight:"bolder" }}>{marketNow}</h5>
        <h4 style={{ textAlign: "center",fontFamily:"times-new-roman",fontWeight:"bolder" }}>{rate2}</h4>
        <h5 style={{ textAlign: "center", color: "yellow",fontFamily:"times-new-roman",fontWeight:"bolder" }}>{foreCast}</h5>
        <h4 style={{ textAlign: "center",fontFamily:"times-new-roman",fontWeight:"bolder" }}>{rate3}</h4>
      </div>
    </div>
  );
};
export default RateItem;
