import React from "react";
import RateItem from "./Item";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

class MandiRates extends React.Component {
  render() {
    const test = [
      {
        city: "Samundri",
        gateRate: "Farm Gate Rate",
        rate: "165 (-30)",
        gurus: "Mandi Gurus",
        rate1: "190 (+30)",
        marketNow: "Market Now",
        rate2: "120 (-30)",
        foreCast: "Forecast",
        rate3: "120 (+30)",
      },
    ];
    return (
      <Container style={{paddingBottom:"50px",paddingTop:"50px"}}>
        <Row>
          <Col>
            {test.map((rate) => (
              <RateItem
                city={rate.city}
                formGateRate={rate.gateRate}
                rate={rate.rate}
                mandiGurus={rate.gurus}
                rate1={rate.rate1}
                marketNow={rate.marketNow}
                rate2={rate.rate2}
                foreCast={rate.foreCast}
                rate3={rate.rate3}
              />
            ))}
          </Col>
          <Col>
            {test.map((rate) => (
              <RateItem
                city={rate.city}
                formGateRate={rate.gateRate}
                rate={rate.rate}
                mandiGurus={rate.gurus}
                rate1={rate.rate1}
                marketNow={rate.marketNow}
                rate2={rate.rate2}
                foreCast={rate.foreCast}
                rate3={rate.rate3}
              />
            ))}
          </Col>
          <Col>
            {test.map((rate) => (
              <RateItem
                city={rate.city}
                formGateRate={rate.gateRate}
                rate={rate.rate}
                mandiGurus={rate.gurus}
                rate1={rate.rate1}
                marketNow={rate.marketNow}
                rate2={rate.rate2}
                foreCast={rate.foreCast}
                rate3={rate.rate3}
              />
            ))}
          </Col>
          <Col>
            {test.map((rate) => (
              <RateItem
                city={rate.city}
                formGateRate={rate.gateRate}
                rate={rate.rate}
                mandiGurus={rate.gurus}
                rate1={rate.rate1}
                marketNow={rate.marketNow}
                rate2={rate.rate2}
                foreCast={rate.foreCast}
                rate3={rate.rate3}
              />
            ))}
          </Col>
        </Row>
        <br />
        <Row>
          <Col>
            {test.map((rate) => (
              <RateItem
                city={rate.city}
                formGateRate={rate.gateRate}
                rate={rate.rate}
                mandiGurus={rate.gurus}
                rate1={rate.rate1}
                marketNow={rate.marketNow}
                rate2={rate.rate2}
                foreCast={rate.foreCast}
                rate3={rate.rate3}
              />
            ))}
          </Col>
          <Col>
            {test.map((rate) => (
              <RateItem
                city={rate.city}
                formGateRate={rate.gateRate}
                rate={rate.rate}
                mandiGurus={rate.gurus}
                rate1={rate.rate1}
                marketNow={rate.marketNow}
                rate2={rate.rate2}
                foreCast={rate.foreCast}
                rate3={rate.rate3}
              />
            ))}
          </Col>
          <Col>
            {test.map((rate) => (
              <RateItem
                city={rate.city}
                formGateRate={rate.gateRate}
                rate={rate.rate}
                mandiGurus={rate.gurus}
                rate1={rate.rate1}
                marketNow={rate.marketNow}
                rate2={rate.rate2}
                foreCast={rate.foreCast}
                rate3={rate.rate3}
              />
            ))}
          </Col>
          <Col>
            {test.map((rate) => (
              <RateItem
                city={rate.city}
                formGateRate={rate.gateRate}
                rate={rate.rate}
                mandiGurus={rate.gurus}
                rate1={rate.rate1}
                marketNow={rate.marketNow}
                rate2={rate.rate2}
                foreCast={rate.foreCast}
                rate3={rate.rate3}
              />
            ))}
          </Col>
        </Row>
      </Container>
    );
  }
}

export default MandiRates;
