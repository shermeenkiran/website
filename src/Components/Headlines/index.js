import React from "react";
import ReactPlayer from "react-player";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Container } from "react-bootstrap";

class Headlines extends React.Component {
  render() {
    return (
      <div style={{ paddingBottom: "50px" }}>
        <Row>
          <Col>
            <h1
              style={{
                color: "white",
                paddingTop: "30px",
                marginTop: "5px",
                marginLeft: "30px",
                fontFamily: "times-new-roman",
              }}
            >
              Recents
            </h1>
          </Col>
        </Row>
        <Row>
          <Col style={{ marginLeft: "35px", paddingTop: "30px" }}>
              <ReactPlayer
                controls
                width="150px"
                height="170px"
                style={{ float: "left" }}
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              ></ReactPlayer>
              <div>
                <h1 className="common-textStyle">Headline</h1>
                <h2 className="common-textStyle">
                  Tagline about the article...
                </h2>
                <p style={{ color: "white" }}>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                  diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                  aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
                  nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                  aliquip ex ea commodo consequat. Duis autem vel eum iriure
                  dolorin hendrerit in vulputate velit esse molestie consequat,
                  vel illum dolore eu feugiat nulla facilisis at...
                </p>
              </div>
          </Col>
        </Row>

        <Row>
          <Col style={{ marginLeft: "35px", paddingTop: "30px" }}>
            <ReactPlayer
              controls
              width="150px"
              height="170px"
              style={{ float: "left" }}
              url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
            ></ReactPlayer>
            <div>
              <h1 className="common-textStyle">Headline</h1>
              <h2 className="common-textStyle">Tagline about the article...</h2>
              <p style={{ color: "white" }}>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
                nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                aliquip ex ea commodo consequat. Duis autem vel eum iriure
                dolorin hendrerit in vulputate velit esse molestie consequat,
                vel illum dolore eu feugiat nulla facilisis at...
              </p>
            </div>
          </Col>
        </Row>

        <Row>
          <Col style={{ marginLeft: "35px", paddingTop: "30px" }}>
            <ReactPlayer
              controls
              width="150px"
              height="170px"
              style={{ float: "left" }}
              url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
            ></ReactPlayer>
            <div>
              <h1 className="common-textStyle">Headline</h1>
              <h2 className="common-textStyle">Tagline about the article...</h2>
              <p style={{ color: "white" }}>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
                nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                aliquip ex ea commodo consequat. Duis autem vel eum iriure
                dolorin hendrerit in vulputate velit esse molestie consequat,
                vel illum dolore eu feugiat nulla facilisis at...
              </p>
            </div>
          </Col>
        </Row>

        <Row>
          <Col style={{ marginLeft: "35px", paddingTop: "30px" }}>
            <ReactPlayer
              controls
              width="150px"
              height="170px"
              style={{ float: "left" }}
              url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
            ></ReactPlayer>
            <div>
              <h1 className="common-textStyle">Headline</h1>
              <h2 className="common-textStyle">Tagline about the article...</h2>
              <p style={{ color: "white" }}>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
                nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                aliquip ex ea commodo consequat. Duis autem vel eum iriure
                dolorin hendrerit in vulputate velit esse molestie consequat,
                vel illum dolore eu feugiat nulla facilisis at...
              </p>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Headlines;
