import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

class Form extends React.Component {
  render() {
    return (
      <div>
        <Row>
          <Col>
            <div className="validationFormMainDiv">
              <div className="formDiv">
                <p
                  style={{
                    alignItems: "center",
                    color: "#390400",
                    marginLeft: "atuo",
                    marginRight: "auto",
                    paddingTop: "50px",
                    fontWeight: "bolder",
                    fontFamily: "times-new-roman",
                    fontSize: "50px",
                    textAlign: "center",
                  }}
                >
                  Ask Poultry Baba
                </p>
                <div style={{ paddingLeft: "30px", paddingRight: "30px" }}>
                  <form>
                    <div
                      style={{ paddingLeft: "20px", paddingRight: "20px" }}
                      class="row"
                    >
                      <div class="col">
                        <input
                          style={{
                            borderRadius: "30px",
                            height: "70px",
                            textAlign: "center",
                          }}
                          type="text"
                          class="form-control"
                          placeholder="FIRST NAME"
                        />
                      </div>
                      <div class="col">
                        <input
                          style={{
                            borderRadius: "30px",
                            height: "70px",
                            textAlign: "center",
                          }}
                          type="text"
                          class="form-control"
                          placeholder="LAST NAME"
                        />
                      </div>
                    </div>
                    <div style={{ paddingTop: "20px" }} class="col">
                      <input
                        style={{
                          borderRadius: "30px",
                          height: "70px",
                          textAlign: "center",
                        }}
                        type="email"
                        class="form-control"
                        placeholder="EMAIL"
                      />
                    </div>
                    <div style={{ paddingTop: "20px" }} class="col">
                      <input
                        style={{
                          borderRadius: "30px",
                          height: "70px",
                          textAlign: "center",
                        }}
                        type="number"
                        class="form-control"
                        placeholder="MOBILE NUMBER"
                      />
                    </div>
                    <div style={{ paddingTop: "20px" }} class="col">
                      <input
                        onClick="askPoultryBaba()"
                        style={{
                          borderRadius: "30px",
                          height: "70px",
                          textAlign: "center",
                        }}
                        type="number"
                        class="form-control"
                        placeholder="WHATS APP NUMBER"
                      />
                    </div>
                    <div style={{ paddingTop: "20px" }} class="col">
                      <input
                        style={{
                          borderRadius: "30px",
                          height: "70px",
                          textAlign: "center",
                        }}
                        type="text"
                        class="form-control"
                        placeholder="CITY"
                      />
                    </div>
                    <div style={{ paddingTop: "20px" }} class="col">
                      <input
                        style={{
                          borderRadius: "30px",
                          height: "250px",
                          textAlign: "center",
                        }}
                        type="textarea"
                        class="form-control"
                        placeholder="YOUR MESSAGE"
                      />
                    </div>
                    <div style={{ paddingTop: "25px" }} class="col">
                      <button type="submit" className="buttonStyling2">
                        Send
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Form;
