import React from "react";
import { Carousel } from "react-responsive-carousel";
import ReactPlayer from "react-player";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

class VideoContainer extends React.Component {
  render() {
    return (
      <div style={{ paddingTop: "50px", paddingBottom: "30px" }}>
        <Container>
          <Row>
            <Col>
              <h4 style={{ textAlign: "left" }}>
                FPCCI hails PM’s poultry-based poverty alleviation move
              </h4>
              <ReactPlayer
                controls
                width="auto"
                height="230px"
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              ></ReactPlayer>
              <p style={{ textAlign: "left" }}>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry business...
              </p>
            </Col>

            <Col>
              <h4 style={{ textAlign: "left" }}>
                FPCCI hails PM’s poultry-based poverty alleviation move
              </h4>
              <ReactPlayer
                controls
                width="auto"
                height="230px"
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              ></ReactPlayer>
              <p style={{ textAlign: "left" }}>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry business...
              </p>
            </Col>

            <Col>
              <h4 style={{ textAlign: "left" }}>
                FPCCI hails PM’s poultry-based poverty alleviation move
              </h4>
              <ReactPlayer
                controls
                width="auto"
                height="230px"
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              ></ReactPlayer>
              <p style={{ textAlign: "left" }}>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry business...
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default VideoContainer;
