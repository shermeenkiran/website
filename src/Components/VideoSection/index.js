import React from "react";
import ReactPlayer from "react-player";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

class VideoSection extends React.Component {
  render() {
    return (
      <div style={{ paddingTop: "100px",paddingBottom:"100px" }}>
        <Container>
          <Row>
            <Col sm={9}>
              <h2>FPCCI hails PM’s poultry-based poverty alleviation move</h2>
              <ReactPlayer
                controls
                width="auto"
                height="250px"
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              ></ReactPlayer>
              <p>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help poor combat poverty through poultry business. READ
                MORE: Punjab CM takes notice of female tourist’s maltreatment in
                Murree
                <br />
                PM’s poultry-based poverty alleviation move should be supported
                as this experience has remained successful in many
                underdeveloped countries, said FPCCI Acting President Karim Aziz
                Malik.
                <br />
                Talking to a delegation of Gujrat Chamber of Commerce led by its
                President Amir Nauman, he said that poultry for the poor is a
                project that should be supported.
                <br />
                Karim Aziz Malik said that economic development is not possible
                in absence of agricultural development for which provision of
                water, cheap energy, loans, linkages with retailers and
                exporters, quality seed and pesticides, reducing the role of
                middlemen and insurance cover is{" "}
              </p>
            </Col>
            <Col sm={3}>
              <div
                style={{ backgroundColor: "darkslateblue", height: "520px" }}
              >
                <marquee
                  style={{
                    color: "whitesmoke",
                    textAlign: "center",
                    fontFamily: "times-new-roman",
                    paddingTop: "200px",
                  }}
                >
                  Poultry Baba App Comming Soon
                </marquee>
              </div>
            </Col>
          </Row>

          <Row>
            <Col>
              <div style={{ backgroundColor: "#390400", height: "5px",marginTop:"100px" }}></div>
            </Col>
          </Row>

          <Row style={{ paddingTop: "100px" }}>
            <Col>
              <h4>FPCCI hails PM’s poultry-based poverty alleviation move</h4>
              <ReactPlayer
                controls
                width="auto"
                height="200px"
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              ></ReactPlayer>
              <p>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry business...
              </p>
            </Col>
            <Col>
              <h4>FPCCI hails PM’s poultry-based poverty alleviation move</h4>
              <ReactPlayer
                controls
                width="auto"
                height="200px"
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              ></ReactPlayer>
              <p>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry business...
              </p>
            </Col>
            <Col>
              <h4>FPCCI hails PM’s poultry-based poverty alleviation move</h4>
              <ReactPlayer
                controls
                width="auto"
                height="200px"
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              ></ReactPlayer>
              <p>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry business...
              </p>
            </Col>
          </Row>

          <Row style={{ paddingTop: "100px" }}>
            <Col>
              <h4>FPCCI hails PM’s poultry-based poverty alleviation move</h4>
              <ReactPlayer
                controls
                width="auto"
                height="200px"
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              ></ReactPlayer>
              <p>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry business...
              </p>
            </Col>
            <Col>
              <h4>FPCCI hails PM’s poultry-based poverty alleviation move</h4>
              <ReactPlayer
                controls
                width="auto"
                height="200px"
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              ></ReactPlayer>
              <p>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry business...
              </p>
            </Col>
            <Col>
              <h4>FPCCI hails PM’s poultry-based poverty alleviation move</h4>
              <ReactPlayer
                controls
                width="auto"
                height="200px"
                url="https://www.youtube.com/watch?v=oROBKfvEh8Y"
              ></ReactPlayer>
              <p>
                The Federation of Pakistan Chambers of Commerce and Industry
                (FPCCI) on Wednesday lauded the move of Prime Minister Imran
                Khan to help the poor combat poverty through poultry business...
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default VideoSection;
