import React from "react";
import { SocialIcon } from "react-social-icons";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

class Header extends React.Component {
  render() {
    return (
      <div style={{ backgroundColor: "#390400",paddingTop:"5px",paddingBottom:"5px" }}>
        <Row>
          <Col sm={9}>
            <SocialIcon url="http://www.youtube.com" />
            <SocialIcon url="http://www.facebook.com" />
            <SocialIcon url="http://instagram.com" />
          </Col>
          <Col sm={3}>
            <input
              style={{
                marginTop:"8px",
                height: "35px",
                backgroundColor: "transparent",
                color: "white",
                border: "1px solid white",
                textAlign: "center",
              }}
              type="text"
              placeholder="Type Here"
            />
            <button
              style={{ backgroundColor: "white", color: "black",height:"40px" }}
              class="btn btn-secondary"
              type="button"
            >
              <i class="fa fa-search"></i>
            </button>
            <button
              style={{
                backgroundColor: "transparent",
                color: "white",
                marginLeft: "10px",
              }}
              class="btn btn-secondary"
              type="button"
            >
              <i class="fa fa-envelope"></i>
            </button>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Header;
